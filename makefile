WJ_MOVIES_BUILD_IMAGE_NAME ?= "wj_flask_movies:latest"

#Build
build:
	docker-compose build

#Test:
test:
	docker run --rm --name movies-app-unit-tests ${WJ_MOVIES_BUILD_IMAGE_NAME} make test-all

#Deploy:
deploy:
	docker-compose up --detach

#Stop All:
stop-all:
	docker-compose stop

#Tear Down All:
tear-down-all:
	docker-compose down
