import json
import logging.config
import socket
import urllib.request
from os import path
import asyncio
import aiohttp
from urllib.error import HTTPError, URLError
from services.movies_config import EXTRACTOR_EXECUTION, API_PROVIDERS

log_file_path = path.join(path.dirname(path.dirname(path.abspath(__file__))), 'logging.ini')
logging.config.fileConfig(log_file_path, disable_existing_loggers=False)
logger = logging.getLogger("movies")


def extract_api_data(url: str, data: dict = None, headers: dict = None,
                     timeout: int = socket._GLOBAL_DEFAULT_TIMEOUT, return_data_type: str = 'json') -> object:
    """
    A simple function to extract data from API Providers

    :param url: resource locator
    :param data: data to be posted along with the Url
    :param headers: additional http request headers such as access token
    :param timeout: timeout for a url request. Defaulted to socket._GLOBAL_DEFAULT_TIMEOUT
    :param return_data_type: The data format type expected to be returned when request is made to the url
    (defaulted to json) Options: json, text, xml. Currently only json is supported
    :return: object (data format specified by data_type input parameter. eg: dict if return_data_type = 'json')
    """

    if data and type(data) == dict:
        data = urllib.parse.urlencode(data)

    req = urllib.request.Request(url, data, headers if headers and type(headers) == dict else dict())

    try:
        response = urllib.request.urlopen(req, timeout=timeout)
        if return_data_type == 'json':
            try:
                return json.loads(response.read().decode())
            except ValueError as error:
                logger.error('API Response error - Invalid Json format.\nAPI URL - {0}\n'
                              'Response Data - {1}\nError Reason - {2}'.format(url, response.read().decode(), error))
    except socket.timeout as st:
        logger.error('API Request timed out.\nAPI URL - {0}\nRequest Data - {1}\nError Reason - {2}'.format(
            url, data, st))
    except (HTTPError, URLError) as error:
        if isinstance(error.reason, socket.timeout):
            logger.error('API Request timed out.\nAPI URL - {0}\nRequest Data - {1}\nError Reason - {2}'.format(
                url, data, error))
        else:
            logger.error('API Request error.\nAPI URL - {0}\nRequest Data - {1}\nError Reason - {2}'.format(
                url, data, error))


async def handle_response_asyncio(response: object, return_data_type: str = 'json'):
    if return_data_type == 'json':
        return await response.json()
    else:
        return await response.text()


async def extract_api_data_async(url: str, data: dict = None, headers: dict = None,
                                 timeout: int = aiohttp.client.DEFAULT_TIMEOUT.total,
                                 return_data_type: str = 'json') -> object:
    """
    A simple function to extract data concurrently from API Providers

    :param url: resource locator
    :param data: data to be posted along with the Url
    :param headers: additional http request headers such as access token
    :param timeout: timeout for a url request. Defaulted to socket._GLOBAL_DEFAULT_TIMEOUT
    :param return_data_type: The data format type expected to be returned when request is made to the url
    (defaulted to json) Options: json, text, xml. Currently only json is supported
    :return: object (data format specified by data_type input parameter. eg: dict if return_data_type = 'json')
    """

    try:
        async with aiohttp.ClientSession(headers=headers, timeout=aiohttp.ClientTimeout(timeout)) as session:
            if data and type(data) == dict:
                async with session.post(url, data=data) as response:
                    try:
                        return await handle_response_asyncio(response, return_data_type=return_data_type)
                    except aiohttp.ClientResponseError as error:
                        logger.error('API Response error.\nAPI URL - {0}\n'
                                     'Response Status - {1}. Error Reason - {2}'.format(url, response.status, error))
            else:
                async with session.get(url) as response:
                    try:
                        return await handle_response_asyncio(response, return_data_type=return_data_type)
                    except aiohttp.ClientResponseError as error:
                        logger.error('API Response error.\nAPI URL - {0}\n'
                                     'Response Status - {1}. Error Reason - {2}'.format(url, response.status, error))
    except asyncio.TimeoutError as st:
        logger.error('API Request timed out.\nAPI URL - {0}\nRequest Data - {1}\nError Reason - {2}'.format(
            url, data, 'Request timed out - Took more than {} seconds to complete the request'.format(timeout)))
    except Exception as error:
        logger.error('API Request error.\nAPI URL - {0}\nRequest Data - {1}\nError Reason - {2}'.format(
                url, data, error))


def extract_all_movies_by_provider(provider: str, provider_details: dict) -> dict:
    """
    A simple function to extract all movies data from a given API Providers

    :param provider: provider name
    :param provider_details: provider details
    :return: dict (containing data provided by the provider)
    """
    all_movies_by_provider = {}
    if not provider_details:
        return all_movies_by_provider
    provider_base_url = provider_details.get('base_url')
    if provider_base_url:
        all_movies_endpoint_url = '{}{}'.format(provider_base_url,
                                                provider_details.get('all_movies_endpoint', {}).get('endpoint_url'))

        all_movies_by_provider = extract_api_data(all_movies_endpoint_url, headers=provider_details.get('url_headers'),
                                                  timeout=provider_details.get('timeout'),
                                                  return_data_type=provider_details.get(
                                                      'all_movies_endpoint', {}).get('return_data_type', 'json'))
        if all_movies_by_provider and type(all_movies_by_provider) == dict:
            all_movies_by_provider['provider'] = provider
    return {} if all_movies_by_provider is None else all_movies_by_provider


async def extract_all_movies_by_provider_async(provider: str, provider_details: dict) -> dict:
    """
    A simple function to extract all movies data from a given API Providers concurrently

    :param provider: provider name
    :param provider_details: provider details
    :return: dict (containing data provided by the provider)
    """
    all_movies_by_provider = {}
    if not provider_details:
        return all_movies_by_provider
    provider_base_url = provider_details.get('base_url')
    if provider_base_url:
        all_movies_endpoint_url = '{}{}'.format(provider_base_url,
                                                provider_details.get('all_movies_endpoint', {}).get('endpoint_url'))

        all_movies_by_provider = await extract_api_data_async(all_movies_endpoint_url,
                                                              headers=provider_details.get('url_headers'),
                                                              timeout=provider_details.get('timeout'),
                                                              return_data_type=provider_details.get(
                                                                  'all_movies_endpoint', {}).get(
                                                                  'return_data_type', 'json'))
        if all_movies_by_provider and type(all_movies_by_provider) == dict:
            all_movies_by_provider['provider'] = provider
    return {} if all_movies_by_provider is None else all_movies_by_provider


def all_unique_movies(all_movies_by_all_providers: dict) -> dict:
    """
    Function to provide unique list of all movies data with movie details from all Providers

    :return: dict (containing list of all unique movies from all the providers)
    """
    unique_movie_dict = {}

    for all_movies_by_provider in all_movies_by_all_providers:
        if all_movies_by_provider and 'Movies' in all_movies_by_provider:
            for movie in all_movies_by_provider.get('Movies', []):
                unique_movie_id = movie.get('Title')
                if unique_movie_id not in unique_movie_dict:
                    unique_movie_dict[unique_movie_id] = {k: movie.get(k, "") for k in ('Title', 'Year',
                                                                                        'Poster', 'Type')}

                if 'Providers' not in unique_movie_dict[unique_movie_id]:
                    unique_movie_dict[unique_movie_id]['Providers'] = [
                        {all_movies_by_provider.get('provider'): {'movie_id': movie.get('ID')}}]
                else:
                    unique_movie_dict[unique_movie_id]['Providers'].append(
                        {all_movies_by_provider.get('provider'): {'movie_id': movie.get('ID')}})
    return unique_movie_dict


def is_cheapest_movie_price_by_provider(cheapest_movie_price, movie_price_by_provider):
    movie_price_by_provider = float(movie_price_by_provider)

    if type(cheapest_movie_price) != float or movie_price_by_provider < cheapest_movie_price:
        return True
    return False


def extract_movies_in_sequence() -> dict:
    """
    Function to extract all movies data from a given API Providers in a sequential execution type

    :return: dict (containing list of all unique movies from all the providers)
    """

    all_movies_by_all_providers = []
    for provider, provider_details in API_PROVIDERS.items():
        all_movies_by_provider = extract_all_movies_by_provider(provider, provider_details)
        all_movies_by_all_providers.append(all_movies_by_provider)

    unique_movies_dict = all_unique_movies(all_movies_by_all_providers)

    for unique_movie, unique_movie_info in unique_movies_dict.items():
        cheapest_price_by_movie_title = {}
        cheapest_price = cheapest_price_by_movie_title.get('cheapest_price', None)
        movie_providers_details = unique_movie_info.get('Providers', []) if unique_movie_info else []

        for provider_details_on_movie in movie_providers_details:
            for provider, movie_info_by_provider in provider_details_on_movie.items():
                movie_details_by_provider = extract_movie_details_by_provider_in_sequence(
                    provider, movie_info_by_provider.get('movie_id', None))
                movie_price_by_provider = movie_details_by_provider.get('Price', None)
                if movie_price_by_provider:
                    try:
                        if is_cheapest_movie_price_by_provider(cheapest_price, movie_price_by_provider):
                            cheapest_price = movie_price_by_provider
                            cheapest_price_by_movie_title['Price'] = cheapest_price
                            cheapest_price_by_movie_title['provider'] = provider
                            cheapest_price_by_movie_title['movie_id'] = movie_details_by_provider.get('ID')
                    except ValueError:
                        logger.error('Price ("{}") provided by "{}" for the movie "{}" is invalid'.format(
                            movie_price_by_provider, provider, movie_details_by_provider.get('Title')))

                movie_info_by_provider['movie_info'] = movie_details_by_provider
        unique_movie_info['Cheapest'] = cheapest_price_by_movie_title
    return unique_movies_dict


async def extract_movies_async():
    """
        Function to extract all movies data concurrently

        :return: dict (containing list of all unique movies from all the providers)
        """

    all_movies_by_all_providers = await asyncio.gather(
        *[extract_all_movies_by_provider_async(provider, provider_details)
          for provider, provider_details in API_PROVIDERS.items()])

    unique_movies_dict = all_unique_movies(all_movies_by_all_providers)

    movie_details_by_provider = await asyncio.gather(*[extract_movie_details_by_provider_async(
        provider, movie_info_by_provider.get('movie_id', None))
        for unique_movie, unique_movie_info in unique_movies_dict.items()
        for provider_details_on_movie in unique_movie_info.get('Providers', [])
        for provider, movie_info_by_provider in provider_details_on_movie.items()])

    cheapest_price_by_movie_title = {}
    for movie_info in movie_details_by_provider:
        provider = movie_info.get('provider')
        movie_title = movie_info.get('Title')
        cheapest_price = cheapest_price_by_movie_title.get('Title', {}).get('cheapest_price', None)
        movie_price_by_provider = movie_info.get('Price', None)
        if movie_price_by_provider:
            try:
                if is_cheapest_movie_price_by_provider(cheapest_price, movie_price_by_provider):
                    cheapest_price = movie_price_by_provider
                    if 'Title' not in cheapest_price_by_movie_title:
                        cheapest_price_by_movie_title[movie_title] = {}
                    cheapest_price_by_movie_title[movie_title]['Price'] = cheapest_price
                    cheapest_price_by_movie_title[movie_title]['provider'] = provider
                    cheapest_price_by_movie_title[movie_title]['movie_id'] = movie_info.get('ID')
            except ValueError:
                logger.error('Price ("{}") provided by "{}" for the movie "{}" is invalid'.format(
                    movie_price_by_provider, provider, movie_info.get('Title')))
        for movie_info_provider in unique_movies_dict.get(movie_title, {}).get('Providers', []):
            if provider in movie_info_provider:
                movie_info_provider[provider]['movie_info'] = movie_info
                break

    for title, cheapest_price_info in cheapest_price_by_movie_title.items():
        unique_movie_details = unique_movies_dict.get(title, {})
        unique_movie_details['Cheapest'] = cheapest_price_info
    return unique_movies_dict


def extract_movies() -> dict:

    """
    Function to extract movies from all providers in an execution type specified in the config

    :return: dict (Returns a dictionary of Movies and the Cheapest price info)
    """

    unique_movies_details = {}
    if EXTRACTOR_EXECUTION.get('execution_type', 'sequence') == 'sequence':
        unique_movies_details = extract_movies_in_sequence()
    elif EXTRACTOR_EXECUTION.get('execution_type', 'sequence') == 'parallel':
        if EXTRACTOR_EXECUTION.get('execution_style', {}).get('style', '') == 'asyncio':
            unique_movies_details = asyncio.run(extract_movies_async())
    return unique_movies_details


def extract_movie_details_by_provider_in_sequence(provider: str, movie_id: dict) -> dict:
    """
    Function to extract a specific movie's details from a given API Provider and the Movie Id known to the provider

    :param provider: provider name
    :param movie_id: Movie Id known to the provider
    :return: dict (containing movie data provided by the provider)
    """
    movie_details_by_provider = {}
    if not provider:
        return movie_details_by_provider
    provider_details = API_PROVIDERS.get(provider, {})
    provider_base_url = provider_details.get('base_url')
    if provider_base_url:
        movie_api_endpoint = provider_details.get('movie_detail_endpoint', {}).get('endpoint_url', '')\
            .format(movie_id)
        movie_details_endpoint_url = '{}{}'.format(provider_base_url, movie_api_endpoint)

        movie_details_by_provider = extract_api_data(movie_details_endpoint_url,
                                                     headers=provider_details.get('url_headers'),
                                                     timeout=provider_details.get('timeout'),
                                                     return_data_type=provider_details.get(
                                                         'movie_detail_endpoint', {}).get('return_data_type', 'json'))
        if movie_details_by_provider and type(movie_details_by_provider) == dict:
            movie_details_by_provider['provider'] = provider
    return {} if movie_details_by_provider is None else movie_details_by_provider


async def extract_movie_details_by_provider_async(provider: str, movie_id: dict) -> dict:
    """
    Function to extract a specific movie's details from a given API Provider and the Movie Id known to the provider
    concurrently

    :param provider: provider name
    :param movie_id: Movie Id known to the provider
    :return: dict (containing movie data provided by the provider)
    """
    movie_details_by_provider = {}
    if not provider:
        return movie_details_by_provider
    provider_details = API_PROVIDERS.get(provider, {})
    provider_base_url = provider_details.get('base_url')
    if provider_base_url:
        movie_api_endpoint = provider_details.get('movie_detail_endpoint', {}).get('endpoint_url', '')\
            .format(movie_id)
        movie_details_endpoint_url = '{}{}'.format(provider_base_url, movie_api_endpoint)

        movie_details_by_provider = await extract_api_data_async(movie_details_endpoint_url,
                                                                 headers=provider_details.get('url_headers'),
                                                                 timeout=provider_details.get('timeout'),
                                                                 return_data_type=provider_details.get(
                                                                     'movie_detail_endpoint', {}).get(
                                                                     'return_data_type', 'json'))
        if movie_details_by_provider and type(movie_details_by_provider) == dict:
            movie_details_by_provider['provider'] = provider
    return {} if movie_details_by_provider is None else movie_details_by_provider


def extract_movie_details(provider: str, movie_id: dict) -> dict:

    """
    Function to extract movie details from a providers in an execution type specified in the config

    :param provider: provider name
    :param movie_id: unique movie_id known to the provider
    :return: dict (Returns a dictionary of Movies and the Cheapest price info)
    """

    unique_movies_details = {}
    if EXTRACTOR_EXECUTION.get('execution_type', 'sequence') == 'sequence':
        unique_movies_details = extract_movie_details_by_provider_in_sequence(provider, movie_id)
    elif EXTRACTOR_EXECUTION.get('execution_type', 'sequence') == 'parallel':
        if EXTRACTOR_EXECUTION.get('execution_style', {}).get('style', '') == 'asyncio':
            unique_movies_details = asyncio.run(extract_movie_details_by_provider_async(provider, movie_id))
    return unique_movies_details
