import os

EXTRACTOR_EXECUTION = {
    'execution_type': 'sequence',
    'execution_style': {
        'style': '',
    }
}

API_PROVIDERS = {
    'cinemaworld': {
        'base_url': os.getenv('MOVIES_PROVIDER_BASE_URL', None),
        'all_movies_endpoint': {
            'endpoint_url': '/api/cinemaworld/movies',
            'return_data_type': 'json'
        },
        'movie_detail_endpoint': {
            'endpoint_url': '/api/cinemaworld/movie/{}',
            'return_data_type': 'json'
        },
        'url_headers': {'x-access-token': os.getenv('MOVIES_X_ACCESS_TOKEN', None)},
        'timeout': 5
    },
    'filmworld': {
        'base_url': os.getenv('MOVIES_PROVIDER_BASE_URL', None),
        'all_movies_endpoint': {
            'endpoint_url': '/api/filmworld/movies',
            'return_data_type': 'json'
        },
        'movie_detail_endpoint': {
            'endpoint_url': '/api/filmworld/movie/{}',
            'return_data_type': 'json'
        },
        'url_headers': {'x-access-token': os.getenv('MOVIES_X_ACCESS_TOKEN', None)},
        'timeout': 5
    }
}
