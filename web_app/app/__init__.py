import os
from flask import Flask


rester_app = Flask(__name__)
rester_app.secret_key = os.getenv('MOVIES_APP_SECRET_KEY')

from app import views

