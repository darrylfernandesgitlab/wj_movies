from flask import redirect, url_for, jsonify
from app import rester_app
from services.movies import extract_movies, extract_movie_details


@rester_app.route('/', methods=['GET'])
def index():
    return redirect(url_for('search_movies'))


@rester_app.route('/Movies', methods=['GET'])
def search_movies():
    data = extract_movies()

    return jsonify(data)


@rester_app.route('/Movies/<provider>/<movie_id>', methods=['GET'])
def movie_details(provider, movie_id):
    data = extract_movie_details(provider=provider, movie_id=movie_id)
    return jsonify(data)
