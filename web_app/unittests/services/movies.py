import unittest
import socket
import aiohttp
import asyncio
import logging
import re
from unittest import mock
from urllib.error import HTTPError, URLError
from aioresponses import aioresponses
from aiohttp.http_exceptions import HttpProcessingError


class TestApiDataExtractor(unittest.TestCase):

    def setUp(self):
        self.baseurl = 'unittest://mocking.com/'

    def test_extract_api_data_having_correct_json_format(self):
        """
            Handles the case wherein if the source url has correct json format data,
            then the extract_api_data method returns a json data
        """
        startendpoint = '/api/provider/movies'
        url = '{}{}'.format(self.baseurl, startendpoint)

        with mock.patch('urllib.request.urlopen') as mock_request:
            mock_response = mock_request.return_value
            mock_response_read = mock_response.read.return_value
            mock_response_read.decode.return_value = '{"a":20, "b":[ {"x": "doll"}, {"x": "hotwheels"} ]}'
            logging.disable(logging.CRITICAL)
            from services.movies import extract_api_data
            json_data = extract_api_data(url, return_data_type='json')
            logging.disable(logging.NOTSET)
            self.assertEqual(json_data, {"a": 20, "b": [{"x": "doll"}, {"x": "hotwheels"}]},
                             msg="Received unexpected json response")

    def test_extract_api_data_having_incorrect_json_format(self):
        """
            Handles the case wherein if the source url has incorrect json format data,
            then the extract_api_data method returns None, but logs the error into the log file for Monitoring purposes
        """
        startendpoint = '/api/provider/movies'
        url = '{}{}'.format(self.baseurl, startendpoint)
        with mock.patch('urllib.request.urlopen') as mock_request:
            mock_response = mock_request.return_value
            mock_response_read = mock_response.read.return_value
            mock_response_read.decode.return_value = 'Some incorrect non-json data format !@#15134*^&abc'
            with self.assertLogs("movies") as ctx:
                from services.movies import extract_api_data
                data = extract_api_data(url, return_data_type='json')
            self.assertTrue(len([log_record for log_record in ctx.records if
                             log_record.name == 'movies' and log_record.funcName == 'extract_api_data'
                             and log_record.levelname == 'ERROR'
                             and 'API Response error - Invalid Json format' in log_record.msg]) > 0,
                            msg="Expected an error log if the API data return type is not json")
            self.assertIsNone(data, msg="Expected None as data type if the API data return type is not json")

    def test_extract_api_data_throwing_httperror(self):
        """
            Handles the case wherein if the source url throws httperror in the response,
            then the extract_api_data method returns None, but logs the error into the log file for Monitoring purposes
        """
        startendpoint = '/api/provider/movies'
        url = '{}{}'.format(self.baseurl, startendpoint)
        with mock.patch('urllib.request.urlopen') as mock_request:
            http_error = HTTPError(url=url, code=500, msg="Internal Server Error", hdrs=None, fp=None)
            mock_request.side_effect = http_error

            with self.assertLogs("movies") as ctx:
                from services.movies import extract_api_data
                data = extract_api_data(url, return_data_type='json')
            self.assertTrue(len([log_record for log_record in ctx.records if
                             log_record.name == 'movies' and log_record.funcName == 'extract_api_data'
                             and log_record.levelname == 'ERROR'
                             and 'API Request error.' in log_record.msg]) > 0,
                            msg="Expected an error log if the API data throws HTTPError")
            self.assertIsNone(data, msg="Expected None as data type if the API data throws HTTPError")

    def test_extract_api_data_throwing_urlerror(self):
        """
            Handles the case wherein if the source url throws urlerror in the response,
            then the extract_api_data method returns None, but logs the error into the log file for Monitoring purposes
        """
        startendpoint = '/api/provider/movies'
        url = '{}{}'.format(self.baseurl, startendpoint)
        with mock.patch('urllib.request.urlopen') as mock_request:
            url_error = URLError(reason="URL Error")
            mock_request.side_effect = url_error

            with self.assertLogs("movies") as ctx:
                from services.movies import extract_api_data
                data = extract_api_data(url, return_data_type='json')
            self.assertTrue(len([log_record for log_record in ctx.records if
                             log_record.name == 'movies' and log_record.funcName == 'extract_api_data'
                             and log_record.levelname == 'ERROR'
                             and 'API Request error.' in log_record.msg]) > 0,
                            msg="Expected an error log if the API data throws URLError")
            self.assertIsNone(data, msg="Expected None as data type if the API data throws URLError")

    def test_extract_api_data_throwing_timeout(self):
        """
            Handles the case wherein if the source url throws timeout in the response,
            then the extract_api_data method returns None, but logs the error into the log file for Monitoring purposes
        """
        startendpoint = '/api/provider/movies'
        url = '{}{}'.format(self.baseurl, startendpoint)
        with mock.patch('urllib.request.urlopen') as mock_request:
            timeout = socket.timeout
            mock_request.side_effect = timeout

            with self.assertLogs("movies") as ctx:
                from services.movies import extract_api_data
                data = extract_api_data(url, return_data_type='json')
            self.assertTrue(len([log_record for log_record in ctx.records if
                             log_record.name == 'movies' and log_record.funcName == 'extract_api_data'
                             and log_record.levelname == 'ERROR'
                             and 'API Request timed out.' in log_record.msg]) > 0,
                            msg="Expected an error log if the API request throws timeout")
            self.assertIsNone(data, msg="Expected None as data type if the API request throws timeout")

    def test_extract_api_data_throwing_httperror_timeout(self):
        """
            Handles the case wherein if the source url throws httperror timeout in the response,
            then the extract_api_data method returns None, but logs the error into the log file for Monitoring purposes
        """
        startendpoint = '/api/provider/movies'
        url = '{}{}'.format(self.baseurl, startendpoint)
        with mock.patch('urllib.request.urlopen') as mock_request:
            http_error = HTTPError(url=url, code=408, msg=socket.timeout(), hdrs=None, fp=None)
            mock_request.side_effect = http_error

            with self.assertLogs("movies") as ctx:
                from services.movies import extract_api_data
                data = extract_api_data(url, return_data_type='json')
            self.assertTrue(len([log_record for log_record in ctx.records if
                             log_record.name == 'movies' and log_record.funcName == 'extract_api_data'
                             and log_record.levelname == 'ERROR'
                             and 'API Request timed out.' in log_record.msg]) > 0,
                            msg="Expected an error log if the API request throws timeout")
            self.assertIsNone(data, msg="Expected None as data type if the API request throws timeout")

    def test_extract_api_data_async_having_correct_json_format(self):
        """
            Handles the case wherein if the source url has correct json format data,
            then the extract_api_data_async method returns a json data
        """
        startendpoint = '/api/provider/movies'
        url = '{}{}'.format(self.baseurl, startendpoint)

        with aioresponses() as mock_aioresponse:
            logging.disable(logging.CRITICAL)

            mock_aioresponse.get(url, payload={"a":20, "b":[ {"x": "doll"}, {"x": "hotwheels"} ]})
            # mock_response_read.return_value ='{"a":20, "b":[ {"x": "doll"}, {"x": "hotwheels"} ]}'
            from services.movies import extract_api_data_async
            json_data = asyncio.run(extract_api_data_async(url, return_data_type='json'))
            logging.disable(logging.NOTSET)
            self.assertEqual(json_data, {"a": 20, "b": [{"x": "doll"}, {"x": "hotwheels"}]},
                             msg="Received unexpected json response")


    def test_extract_api_data_async_throwing_server_side_error(self):
        """
            Handles the case wherein if the source url has incorrect json format data,
            then the extract_api_data_async method returns None, but logs the error into the log file for Monitoring purposes
        """
        startendpoint = '/api/provider/movies'
        url = '{}{}'.format(self.baseurl, startendpoint)
        with aioresponses() as mock_aioresponse:
            mock_aioresponse.get(url, status=503, reason='Service Unavailable', exception=HttpProcessingError())
            # mock_response_read.return_value ='{"a":20, "b":[ {"x": "doll"}, {"x": "hotwheels"} ]}'
            from services.movies import extract_api_data_async

            with self.assertLogs("movies") as ctx:
                from services.movies import extract_api_data
                json_data = asyncio.run(extract_api_data_async(url, return_data_type='json'))
                self.assertTrue(len([log_record for log_record in ctx.records if
                                     log_record.name == 'movies' and log_record.funcName == 'extract_api_data_async'
                                     and log_record.levelname == 'ERROR'
                                     and 'API Request error' in log_record.msg]) > 0,
                                msg="Expected an error log if the API throws Error")
                self.assertIsNone(json_data, msg="Expected None as data type if the API data return type is not json")

    def test_extract_api_data_async_throwing_timeout_error(self):
        """
            Handles the case wherein if the source url throws timeout in the response,
            then the extract_api_data method returns None, but logs the error into the log file for Monitoring purposes
        """
        startendpoint = '/api/provider/movies'
        url = '{}{}'.format(self.baseurl, startendpoint)
        with aioresponses() as mock_aioresponse:
            mock_aioresponse.get(url, reason='Request Timeout', exception=asyncio.TimeoutError())
            # mock_response_read.return_value ='{"a":20, "b":[ {"x": "doll"}, {"x": "hotwheels"} ]}'
            from services.movies import extract_api_data_async

            with self.assertLogs("movies") as ctx:
                from services.movies import extract_api_data
                json_data = asyncio.run(extract_api_data_async(url, return_data_type='json'))
                self.assertTrue(len([log_record for log_record in ctx.records if
                                     log_record.name == 'movies' and log_record.funcName == 'extract_api_data_async'
                                     and log_record.levelname == 'ERROR'
                                     and 'API Request timed out' in log_record.msg]) > 0,
                                msg="Expected an error log if the API throws TimeoutError")
                self.assertIsNone(json_data, msg="Expected None as data type if the API data return type is not json")


def mocked_extract_all_movies_by_provider_with_data(provider, provider_details):
    if provider_details.get('all_movies_endpoint', {}).get('endpoint_url') == '/api/filmworld/movies':
        return {'Movies': []}
    if provider_details.get('all_movies_endpoint', {}).get('endpoint_url') == '/api/cinemaworld/movies':
        return {'Movies': [
                {'Title': 'Star Wars: Episode IV - A New Hope', 'Year': '1977', 'ID': 'cw0076759', 'Type': 'movie',
                 'Poster': 'photo1.jpg'},
                {'Title': 'Star Wars: Episode V - The Empire Strikes Back', 'Year': '1980', 'ID': 'cw0080684',
                 'Type': 'movie', 'Poster': 'photo2.jpg'},
                ]}
    return None


def mocked_extract_all_movies_by_provider_async_with_data(provider, provider_details):
    if provider_details.get('all_movies_endpoint', {}).get('endpoint_url') == '/api/filmworld/movies':
        return {'Movies': []}
    if provider_details.get('all_movies_endpoint', {}).get('endpoint_url') == '/api/cinemaworld/movies':
        return {'Movies': [
                {'Title': 'Star Wars: Episode IV - A New Hope', 'Year': '1977', 'ID': 'cw0076759', 'Type': 'movie',
                 'Poster': 'photo1.jpg'},
                {'Title': 'Star Wars: Episode V - The Empire Strikes Back', 'Year': '1980', 'ID': 'cw0080684',
                 'Type': 'movie', 'Poster': 'photo2.jpg'},
                ]}
    return None


def mocked_extract_all_movies_by_provider_with_no_data(provider, provider_details):
    return None


def mocked_extract_all_movies_by_provider_async_with_no_data(provider, provider_details):
    return None


class TestExtractMovies(unittest.TestCase):
    def setUp(self):
        self.MOCK_API_PROVIDERS = {
            'cinemaworld': {
                'base_url': 'unittest://mocking.com/',
                'all_movies_endpoint': {
                    'endpoint_url': '/api/cinemaworld/movies',
                    'return_data_type': 'json'
                },
                'movie_detail_endpoint': {
                    'endpoint_url': '/api/cinemaworld/movie/{}',
                    'return_data_type': 'json'
                },
                'url_headers': {'x-access-token': 'faketoken1'},
                'timeout': 2
            },
            'filmworld': {
                'base_url': 'unittest://mocking.com/',
                'all_movies_endpoint': {
                    'endpoint_url': '/api/filmworld/movies',
                    'return_data_type': 'json'
                },
                'movie_detail_endpoint': {
                    'endpoint_url': '/api/filmworld/movies',
                    'return_data_type': 'json'
                },
                'url_headers': {'x-access-token': 'faketoken2'},
                'timeout': 2
            }
        }

    def test_extract_all_movies_by_provider_having_no_provider_url(self):
        """
            Handles the case wherein no source provider details are provide to the extract_all_movies_by_provider method
        """
        logging.disable(logging.CRITICAL)
        mock_provider_details = {
        }
        from services.movies import extract_all_movies_by_provider
        all_movies_by_provider = extract_all_movies_by_provider(provider=None, provider_details=mock_provider_details)
        self.assertFalse(all_movies_by_provider, msg='Expected empty dictionary as an output '
                                                     'if no provider details are provided')
        mock_provider_details = {
            'base_url': None,
        }
        all_movies_by_provider = extract_all_movies_by_provider(provider='cinemaworld',
                                                                provider_details=mock_provider_details)
        self.assertFalse(all_movies_by_provider, msg='Expected empty dictonary as an output '
                                                     'if no provider details are provided')

        mock_provider_details = {
            'base_url': '',
        }
        all_movies_by_provider = extract_all_movies_by_provider(provider='cinemaworld',
                                                                provider_details=mock_provider_details)
        self.assertFalse(all_movies_by_provider, msg='Expected empty dictionary as an output if an empty string '
                                                     'is provided as the base_url in provider details')
        logging.disable(logging.NOTSET)

    def test_extract_all_movies_by_provider_having_correct_provider_details(self):
        """
            Handles the case wherein if correct source provider details are provided to the
            extract_all_movies_by_provider method
        """
        logging.disable(logging.CRITICAL)

        with mock.patch('services.movies.extract_api_data') as mock_extract_api_data:
            mock_extract_api_data.return_value = {'Movies': [
                {'Title': 'Star Wars: Episode IV - A New Hope', 'Year': '1977', 'ID': 'cw0076759', 'Type': 'movie',
                 'Poster': 'photo1.jpg'},
                {'Title': 'Star Wars: Episode V - The Empire Strikes Back', 'Year': '1980', 'ID': 'cw0080684', 'Type': 'movie',
                 'Poster': 'photo2.jpg'},
                ]}
            from services.movies import extract_all_movies_by_provider
            all_movies_by_provider = extract_all_movies_by_provider(provider='cinemaworld',
                                                                    provider_details=self.MOCK_API_PROVIDERS.get(
                                                                        'cinemaworld'))
            self.assertIsNotNone(all_movies_by_provider, msg="Expected a not None object returned by "
                                                             "extract_all_movies_by_provider function")

            mock_extract_api_data.return_value = None
            all_movies_by_provider = extract_all_movies_by_provider(provider='cinemaworld',
                                                                    provider_details=self.MOCK_API_PROVIDERS.get(
                                                                        'cinemaworld'))
            self.assertFalse(all_movies_by_provider,
                             msg="Expected an empty dictionary returned by extract_all_movies_by_provider function"
                                 "when no data is provided by the provider")
        logging.disable(logging.NOTSET)

    def test_extract_all_movies_by_provider_async_having_no_provider_url(self):
        """
            Handles the case wherein no source provider details are provide to the
            extract_all_movies_by_provider_async method
        """
        logging.disable(logging.CRITICAL)
        mock_provider_details = {
        }
        from services.movies import extract_all_movies_by_provider_async
        all_movies_by_provider = asyncio.run(extract_all_movies_by_provider_async(provider=None, provider_details=mock_provider_details))
        self.assertFalse(all_movies_by_provider, msg='Expected empty dictionary as an output '
                                                     'from extract_all_movies_by_provider_async'
                                                     'if no provider details are provided')
        mock_provider_details = {
            'base_url': None,
        }
        all_movies_by_provider = asyncio.run(extract_all_movies_by_provider_async(
            provider='cinemaworld', provider_details=mock_provider_details))
        self.assertFalse(all_movies_by_provider, msg='Expected empty dictionary as an output '
                                                     'from extract_all_movies_by_provider_async'                                                     
                                                     'if no provider details are provided')

        mock_provider_details = {
            'base_url': '',
        }
        all_movies_by_provider = asyncio.run(extract_all_movies_by_provider_async(
            provider='cinemaworld', provider_details=mock_provider_details))
        self.assertFalse(all_movies_by_provider, msg='Expected empty dictionary as an output '
                                                     'from extract_all_movies_by_provider_asyncif an empty string '
                                                     'is provided as the base_url in provider details')
        logging.disable(logging.NOTSET)

    def test_extract_all_movies_by_provider_async_having_correct_provider_details(self):
        """
            Handles the case wherein if correct source provider details are provided to the
            extract_all_movies_by_provider_async method
        """
        logging.disable(logging.CRITICAL)

        with mock.patch('services.movies.extract_api_data_async') as mock_extract_api_data_async:
            mock_extract_api_data_async.return_value = {'Movies': [
                {'Title': 'Star Wars: Episode IV - A New Hope', 'Year': '1977', 'ID': 'cw0076759', 'Type': 'movie',
                 'Poster': 'photo1.jpg'},
                {'Title': 'Star Wars: Episode V - The Empire Strikes Back', 'Year': '1980', 'ID': 'cw0080684', 'Type': 'movie',
                 'Poster': 'photo2.jpg'},
                ]}
            from services.movies import extract_all_movies_by_provider_async
            all_movies_by_provider = asyncio.run(extract_all_movies_by_provider_async(
                provider='cinemaworld', provider_details=self.MOCK_API_PROVIDERS.get('cinemaworld')))
            self.assertIsNotNone(all_movies_by_provider, msg="Expected a not None object returned by "
                                                             "extract_all_movies_by_provider function")

            mock_extract_api_data_async.return_value = None
            all_movies_by_provider = asyncio.run(extract_all_movies_by_provider_async(
                provider='cinemaworld', provider_details=self.MOCK_API_PROVIDERS.get('cinemaworld')))
            self.assertFalse(all_movies_by_provider,
                             msg="Expected an empty dictionary returned by extract_all_movies_by_provider_async "
                                 "function when no data is provided by the provider")
        logging.disable(logging.NOTSET)

    def test_extract_movies_in_sequence_having_no_data_from_providers(self):
        """
            Handles the case wherein no data is provided from the providers when extract_movies_in_sequence is executed
        """

        logging.disable(logging.CRITICAL)
        with mock.patch('services.movies.API_PROVIDERS', self.MOCK_API_PROVIDERS):
            with mock.patch('services.movies.extract_all_movies_by_provider',
                            side_effect=mocked_extract_all_movies_by_provider_with_no_data, autospec=True):
                from services.movies import extract_movies_in_sequence
                movies = extract_movies_in_sequence()
                self.assertFalse(movies, msg='Expected empty dictionary as an output from '
                                             'extract_movies_in_sequence when none of the providers '
                                             'provide any data')
        logging.disable(logging.NOTSET)

    def test_extract_movies_in_sequence_having_data_from_providers(self):
        """
            Handles the case wherein data is provided from the providers when extract_movies_in_sequence is executed
        """

        logging.disable(logging.CRITICAL)
        with mock.patch('services.movies.API_PROVIDERS', self.MOCK_API_PROVIDERS):
            with mock.patch('services.movies.extract_all_movies_by_provider',
                            side_effect=mocked_extract_all_movies_by_provider_with_data, autospec=True):
                from services.movies import extract_movies_in_sequence
                movies = extract_movies_in_sequence()
                self.assertTrue(movies, msg='Expected non empty dictionary as an output from '
                                            'extract_movies_in_sequence when providers '
                                            'provide any data')
                self.assertIsInstance(movies, dict, msg='Expected dictionary as an output from '
                                                        'extract_movies_in_sequence when providers '
                                                        'provide any data')

        logging.disable(logging.NOTSET)

    def test_extract_movies_async_having_no_data_from_providers(self):
        """
            Handles the case wherein no data is provided from the providers when extract_movies_async is executed
        """

        logging.disable(logging.CRITICAL)
        with mock.patch('services.movies.API_PROVIDERS', self.MOCK_API_PROVIDERS):
            with mock.patch('services.movies.extract_all_movies_by_provider_async',
                            side_effect=mocked_extract_all_movies_by_provider_async_with_no_data, autospec=True):
                from services.movies import extract_movies_async
                movies = asyncio.run(extract_movies_async())
                self.assertFalse(movies, msg='Expected empty dictionary as an output from '
                                             'extract_movies_async when none of the providers '
                                             'provide any data')
        logging.disable(logging.NOTSET)

    def test_extract_movies_async_having_data_from_providers(self):
        """
            Handles the case wherein data is provided from the providers when extract_movies_in_sequence is executed
        """

        logging.disable(logging.CRITICAL)
        with mock.patch('services.movies.API_PROVIDERS', self.MOCK_API_PROVIDERS):
            with mock.patch('services.movies.extract_all_movies_by_provider_async',
                            side_effect=mocked_extract_all_movies_by_provider_async_with_data, autospec=True):
                from services.movies import extract_movies_async
                movies = asyncio.run(extract_movies_async())
                self.assertTrue(movies, msg='Expected non empty dictionary as an output from '
                                            'extract_movies_async when providers '
                                            'provide any data')
                self.assertIsInstance(movies, dict, msg='Expected dictionary as an output from '
                                                        'extract_movies_async when providers '
                                                        'provide any data')

        logging.disable(logging.NOTSET)

    def test_extract_movies_having_config_set_to_sequential_execution(self):
        """
            Handles the case wherein if the extract_movies method is handled in sequence
        """
        logging.disable(logging.CRITICAL)

        MOCK_EXTRACTOR_EXECUTION = {
            'execution_type': 'sequence',
        }
        with mock.patch('services.movies.EXTRACTOR_EXECUTION', MOCK_EXTRACTOR_EXECUTION):
            with mock.patch('services.movies.API_PROVIDERS', self.MOCK_API_PROVIDERS):
                with mock.patch('services.movies.extract_all_movies_by_provider',
                                side_effect=mocked_extract_all_movies_by_provider_with_no_data, autospec=True):
                    from services.movies import extract_movies
                    movies = extract_movies()
                    self.assertFalse(movies, msg='Expected empty dictionary as an output from '
                                                 'extract_movies_in_sequence when none of the providers '
                                                 'provide any data')

                with mock.patch('services.movies.extract_all_movies_by_provider',
                                side_effect=mocked_extract_all_movies_by_provider_with_data, autospec=True):
                    from services.movies import extract_movies
                    movies = extract_movies()
                    self.assertTrue(movies, msg='Expected non empty dictionary as an output from '
                                                 'extract_movies_in_sequence when providers '
                                                 'provide any data')
                    self.assertIsInstance(movies, dict, msg='Expected dictionary as an output from '
                                                'extract_movies_in_sequence when providers '
                                                'provide any data')

        logging.disable(logging.NOTSET)

    def test_extract_movies_having_config_set_to_parallel_execution(self):
        """
            Handles the case wherein if the extract_movies method is handled in parallel
        """
        logging.disable(logging.CRITICAL)

        MOCK_EXTRACTOR_EXECUTION = {
            'execution_type': 'parallel',
            'execution_style': {
                'style': 'asyncio',
            }
        }
        with mock.patch('services.movies.EXTRACTOR_EXECUTION', MOCK_EXTRACTOR_EXECUTION):
            with mock.patch('services.movies.API_PROVIDERS', self.MOCK_API_PROVIDERS):
                with mock.patch('services.movies.extract_all_movies_by_provider_async',
                                side_effect=mocked_extract_all_movies_by_provider_async_with_no_data, autospec=True):
                    from services.movies import extract_movies
                    movies = extract_movies()
                    self.assertFalse(movies, msg='Expected empty dictionary as an output from '
                                                 'extract_movies when none of the providers '
                                                 'provide any data and execution type is parallel')

                with mock.patch('services.movies.extract_all_movies_by_provider_async',
                                side_effect=mocked_extract_all_movies_by_provider_async_with_data, autospec=True):
                    from services.movies import extract_movies
                    movies = extract_movies()
                    self.assertTrue(movies, msg='Expected non empty dictionary as an output from '
                                                'extract_movies method when providers '
                                                'provide any data and execution type is parallel')
                    self.assertIsInstance(movies, dict, msg='Expected dictionary as an output from '
                                                            'extract_movies method when providers '
                                                            'provide any data and execution type is parallel')

        logging.disable(logging.NOTSET)


def mocked_movie_details_from_api_provider_having_no_data(url: str, data: dict = None, headers: dict = None,
                                                          timeout: int = socket._GLOBAL_DEFAULT_TIMEOUT,
                                                          return_data_type: str = 'json') -> object:
    return None


def mocked_movie_details_from_api_provider_async_having_no_data(url: str, data: dict = None, headers: dict = None,
                                                                timeout: int = aiohttp.client.DEFAULT_TIMEOUT.total,
                                                                return_data_type: str = 'json') -> object:
    return None


def mocked_movie_details_from_api_provider_having_data(url: str, data: dict = None, headers: dict = None,
                                                       timeout: int = socket._GLOBAL_DEFAULT_TIMEOUT,
                                                       return_data_type: str = 'json') -> object:
    if '/api/cinemaworld/movie/' in url:
        return {'Title': 'Star Wars: Episode IV - A New Hope', 'Year': '1977', 'Rated': 'PG',
                'Released': '25 May 1977', 'Runtime': '121 min', 'Genre': 'Action, Adventure, Fantasy',
                'Director': 'George Lucas', 'Writer': 'George Lucas',
                'Actors': 'Mark Hamill, Harrison Ford, Carrie Fisher, Peter Cushing',
                'Plot': "Luke Skywalker joins forces with a Jedi Knight, a cocky pilot, a wookiee and two "
                        "droids to save the galaxy from the Empire's world-destroying battle-station, "
                        "while also attempting to rescue Princess Leia from the evil Darth Vader.",
                'Language': 'English', 'Country': 'USA', 'Awards': 'Won 6 Oscars. Another 48 wins & 28 nominations.',
                'Poster': 'photo1.jpg', 'Metascore': '92', 'Rating': '8.7', 'Votes': '915,459',
                'ID': 'FAKE_ID_1', 'Type': 'movie', 'Price': '123.5'}

    if '/api/filmworld/movie/' in url:
        return {}
    return None


def mocked_movie_details_from_api_provider_async_having_data(url: str, data: dict = None, headers: dict = None,
                                                             timeout: int = aiohttp.client.DEFAULT_TIMEOUT.total,
                                                             return_data_type: str = 'json') -> object:
    if '/api/cinemaworld/movie/' in url:
        return {'Title': 'Star Wars: Episode IV - A New Hope', 'Year': '1977', 'Rated': 'PG',
                'Released': '25 May 1977', 'Runtime': '121 min', 'Genre': 'Action, Adventure, Fantasy',
                'Director': 'George Lucas', 'Writer': 'George Lucas',
                'Actors': 'Mark Hamill, Harrison Ford, Carrie Fisher, Peter Cushing',
                'Plot': "Luke Skywalker joins forces with a Jedi Knight, a cocky pilot, a wookiee and two "
                        "droids to save the galaxy from the Empire's world-destroying battle-station, "
                        "while also attempting to rescue Princess Leia from the evil Darth Vader.",
                'Language': 'English', 'Country': 'USA', 'Awards': 'Won 6 Oscars. Another 48 wins & 28 nominations.',
                'Poster': 'photo1.jpg', 'Metascore': '92', 'Rating': '8.7', 'Votes': '915,459',
                'ID': 'FAKE_ID_1', 'Type': 'movie', 'Price': '123.5'}

    if '/api/filmworld/movie/' in url:
        return {}
    return None


class TestExtractMovieDetails(unittest.TestCase):
    def setUp(self):
        self.MOCK_API_PROVIDERS = {
            'cinemaworld': {
                'base_url': 'unittest://mocking.com/',
                'all_movies_endpoint': {
                    'endpoint_url': '/api/cinemaworld/movies',
                    'return_data_type': 'json'
                },
                'movie_detail_endpoint': {
                    'endpoint_url': '/api/cinemaworld/movie/{}',
                    'return_data_type': 'json'
                },
                'url_headers': {'x-access-token': 'faketoken1'},
                'timeout': 2
            },
            'filmworld': {
                'base_url': 'unittest://mocking.com/',
                'all_movies_endpoint': {
                    'endpoint_url': '/api/filmworld/movies',
                    'return_data_type': 'json'
                },
                'movie_detail_endpoint': {
                    'endpoint_url': '/api/filmworld/movies',
                    'return_data_type': 'json'
                },
                'url_headers': {'x-access-token': 'faketoken2'},
                'timeout': 2
            }
        }

    def test_extract_movie_details_by_provider_in_sequence_having_no_provider_or_movie_id(self):
        logging.disable(logging.CRITICAL)
        with mock.patch('services.movies.API_PROVIDERS', self.MOCK_API_PROVIDERS):
            from services.movies import extract_movie_details_by_provider_in_sequence
            movie_details_by_provider = extract_movie_details_by_provider_in_sequence(provider=None, movie_id=None)
            self.assertFalse(movie_details_by_provider,
                             msg='Expected empty dictionary as an output from extract_movie_details_by_provider '
                                 'when no provider and movie id is provided')
            self.assertIsInstance(movie_details_by_provider, dict,
                                  msg='Expected dictionary as an output from extract_movie_details_by_provider '
                                      'when no provider and movie id is provided')

            with mock.patch('services.movies.extract_api_data',
                            side_effect=mocked_movie_details_from_api_provider_having_no_data, autospec=True):
                from services.movies import extract_movie_details_by_provider_in_sequence
                movie_details_by_provider = extract_movie_details_by_provider_in_sequence(provider='cinemaworld', movie_id=None)
                self.assertFalse(movie_details_by_provider,
                                 msg='Expected empty dictionary as an output from '
                                     'extract_movie_details_by_provider_in_sequence method when no movie id is provided')
                self.assertIsInstance(movie_details_by_provider, dict,
                                      msg='Expected dictionary as an output from '
                                          'extract_movie_details_by_provider_in_sequence method when when no movie id is provided')
        logging.disable(logging.NOTSET)

    def test_extract_movie_details_by_provider_in_sequence_having_provider_and_movie_id(self):
        logging.disable(logging.CRITICAL)
        with mock.patch('services.movies.API_PROVIDERS', self.MOCK_API_PROVIDERS):
            with mock.patch('services.movies.extract_api_data',
                            side_effect=mocked_movie_details_from_api_provider_having_data, autospec=True):
                from services.movies import extract_movie_details_by_provider_in_sequence
                movie_details_by_provider = extract_movie_details_by_provider_in_sequence(provider='cinemaworld',
                                                                                          movie_id='FAKE_ID_1')
                self.assertTrue(movie_details_by_provider,
                                msg='Expected dictionary as an output containing movie details from '
                                    'extract_movie_details_by_provider_in_sequence method')
                self.assertIsInstance(movie_details_by_provider, dict,
                                      msg='Expected dictionary as an output from '
                                          'extract_movie_details_by_provider_in_sequence method')

        logging.disable(logging.NOTSET)

    def test_extract_movie_details_by_provider_async_having_no_provider_or_movie_id(self):
        logging.disable(logging.CRITICAL)
        with mock.patch('services.movies.API_PROVIDERS', self.MOCK_API_PROVIDERS):
            from services.movies import extract_movie_details_by_provider_async
            movie_details_by_provider = asyncio.run(extract_movie_details_by_provider_async(provider=None, movie_id=None))
            self.assertFalse(movie_details_by_provider,
                             msg='Expected empty dictionary as an output from extract_movie_details_by_provider_async '
                                 'when no provider and movie id is provided')
            self.assertIsInstance(movie_details_by_provider, dict,
                                  msg='Expected dictionary as an output from extract_movie_details_by_provider_async '
                                      'when no provider and movie id is provided')

            with mock.patch('services.movies.extract_api_data_async',
                            side_effect=mocked_movie_details_from_api_provider_async_having_no_data, autospec=True):
                from services.movies import extract_movie_details_by_provider_async
                movie_details_by_provider = asyncio.run(extract_movie_details_by_provider_async(
                    provider='cinemaworld', movie_id=None))
                self.assertFalse(movie_details_by_provider,
                                 msg='Expected empty dictionary as an output from '
                                     'extract_movie_details_by_provider_async method when no movie id is provided')
                self.assertIsInstance(movie_details_by_provider, dict,
                                      msg='Expected dictionary as an output from '
                                          'extract_movie_details_by_provider_async method when when no movie id is provided')
        logging.disable(logging.NOTSET)

    def test_extract_movie_details_by_provider_async_having_provider_and_movie_id(self):
        logging.disable(logging.CRITICAL)
        with mock.patch('services.movies.API_PROVIDERS', self.MOCK_API_PROVIDERS):
            with mock.patch('services.movies.extract_api_data_async',
                            side_effect=mocked_movie_details_from_api_provider_async_having_data, autospec=True):
                from services.movies import extract_movie_details_by_provider_async
                movie_details_by_provider = asyncio.run(extract_movie_details_by_provider_async(
                    provider='cinemaworld', movie_id='FAKE_ID_1'))
                self.assertTrue(movie_details_by_provider,
                                msg='Expected dictionary as an output containing movie details from '
                                    'extract_movie_details_by_provider_async method')
                self.assertIsInstance(movie_details_by_provider, dict,
                                      msg='Expected dictionary as an output from '
                                          'extract_movie_details_by_provider_async method')

        logging.disable(logging.NOTSET)

    def test_extract_movie_details_having_config_set_to_sequential_execution(self):
        logging.disable(logging.CRITICAL)
        MOCK_EXTRACTOR_EXECUTION = {
            'execution_type': 'sequence',
        }
        with mock.patch('services.movies.EXTRACTOR_EXECUTION', MOCK_EXTRACTOR_EXECUTION):
            with mock.patch('services.movies.API_PROVIDERS', self.MOCK_API_PROVIDERS):
                from services.movies import extract_movie_details
                movie_details = extract_movie_details(provider=None, movie_id=None)
                self.assertFalse(movie_details,
                                 msg='Expected empty dictionary as an output from extract_movie_details '
                                     'using sequential execution when no provider and movie id is provided')
                self.assertIsInstance(movie_details, dict,
                                      msg='Expected dictionary as an output from extract_movie_details '
                                          'using sequential execution when no provider and movie id is provided')

                with mock.patch('services.movies.extract_api_data',
                                side_effect=mocked_movie_details_from_api_provider_having_no_data, autospec=True):
                    from services.movies import extract_movie_details
                    movie_details = extract_movie_details(provider='cinemaworld', movie_id=None)
                    self.assertFalse(movie_details,
                                     msg='Expected empty dictionary as an output from '
                                         'extract_movie_details method using sequential execution when no movie id is provided')
                    self.assertIsInstance(movie_details, dict,
                                          msg='Expected dictionary as an output from '
                                              'extract_movie_details method using sequential execution when '
                                              'no movie id is provided')

                with mock.patch('services.movies.extract_api_data',
                                side_effect=mocked_movie_details_from_api_provider_having_data, autospec=True):
                    from services.movies import extract_movie_details
                    movie_details = extract_movie_details(provider='cinemaworld',
                                                                                  movie_id='FAKE_ID_1')
                    self.assertTrue(movie_details,
                                    msg='Expected dictionary as an output containing movie details from '
                                        'extract_movie_details method using sequential execution')
                    self.assertIsInstance(movie_details, dict,
                                          msg='Expected dictionary as an output from '
                                              'extract_movie_details method using sequential execution')

        logging.disable(logging.NOTSET)

    def test_extract_movie_details_having_config_set_to_parallel_execution(self):
        logging.disable(logging.CRITICAL)
        MOCK_EXTRACTOR_EXECUTION = {
            'execution_type': 'parallel',
            'execution_style': {
                'style': 'asyncio',
            }
        }
        with mock.patch('services.movies.EXTRACTOR_EXECUTION', MOCK_EXTRACTOR_EXECUTION):
            with mock.patch('services.movies.API_PROVIDERS', self.MOCK_API_PROVIDERS):
                with mock.patch('services.movies.extract_api_data_async',
                                side_effect=mocked_movie_details_from_api_provider_async_having_data, autospec=True):
                    from services.movies import extract_movie_details
                    movie_details = extract_movie_details(provider='cinemaworld', movie_id='FAKE_ID_1')
                    self.assertTrue(movie_details,
                                    msg='Expected dictionary as an output containing movie details from '
                                        'extract_movie_details method using parallel execution')
                    self.assertIsInstance(movie_details, dict,
                                          msg='Expected dictionary as an output from '
                                              'extract_movie_details method using parallel execution')

            logging.disable(logging.NOTSET)


if __name__ == '__main__':
    unittest.main()
